use futures::prelude::*;
use influxdb2::models::DataPoint;
use influxdb2::Client;
use log::{debug, error, info};
use rumqttc::Event;
use rumqttc::{AsyncClient, Incoming, MqttOptions, Publish, QoS};
use serde::Deserialize;
use serde_json::json;
use std::fs::File;
use std::io::Read;
use std::sync::Arc;
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::sync::Mutex;
use tokio::time::Duration;
use tokio::{task, time};

static ESP_TOPIC: &str = "esp32/accel";


#[tokio::main]
async fn main() {
    env_logger::init();

    let token_file = match std::env::var("TOKEN_FILE") {
        Ok(value) => value,
        Err(_) => panic!("Failed to get token file exiting"),
    };
    info!("token-file is  {}", token_file);

    let mut file = File::open(token_file).unwrap();
    let mut token: String = "".to_string();
    file.read_to_string(&mut token).unwrap();

    token = token.trim().to_string();
    info!("token retrieved as {}", token);

    // let host = std::env::var("INFLUXDB_HOST").unwrap();
    let host = env!("INFLUXDB_HOST");
    // let org = std::env::var("INFLUXDB_ORG").unwrap();
    let org = env!("INFLUXDB_ORG");
    let influx_client = Arc::new(Mutex::new(Client::new(host, org, &token)));

    'main: loop {
        let mut mqttoptions = MqttOptions::new(env!("MQTT_ANNOUNCE_NAME"), env!("MQTT_HOST"), 1883);
        mqttoptions.set_keep_alive(Duration::from_secs(5));

        let (client, mut eventloop) = AsyncClient::new(mqttoptions, 10);
        client
            .subscribe(
                ESP_TOPIC,
                QoS::AtMostOnce,
            )
            .await
            .unwrap();




        // keep_alive_loops.await;

        loop {
            let notification = match eventloop.poll().await {
                Ok(notifaction) => notifaction,
                Err(err) => {
                    dbg!(err);
                    error!("connection to mqtt lost attempting to reconnect");
                    continue 'main;
                }
            };

            match notification {
                Event::Incoming(packet) => match packet {
                    Incoming::Connect(_) => {}
                    Incoming::ConnAck(_) => {}
                    Incoming::Publish(publish) => {
                        debug!("Get timestamp");
                        let Some(time_received) = SystemTime::now()
                            .duration_since(UNIX_EPOCH).ok()
                            else {
                                error!("Unable to compute time stamp");
                                error!("nothing sent to influx");
                                return;
                            };

                        debug!("convert timestamp to correct type");
                        let Some(time_received): Option<i64> = time_received.as_nanos().try_into().ok() else {
                            error!("unable to convert timestamp into nanos");
                            error!("nothing sent to influx");
                            return;
                        };

                        debug!("recived {}, from {}", std::str::from_utf8(&publish.payload).unwrap(), publish.topic);


                        if publish.topic == ESP_TOPIC{
                            let c_influx_client = influx_client.clone();
                            task::spawn(async move {
                                handle_publish(publish, c_influx_client, time_received, "battery", "current", "V")
                                    .await;
                            });
                        }
                    }
                    Incoming::PubAck(_) => {}
                    Incoming::PubRec(_) => {}
                    Incoming::PubRel(_) => {}
                    Incoming::PubComp(_) => {}
                    Incoming::Subscribe(_) => {}
                    Incoming::SubAck(_) => {}
                    Incoming::Unsubscribe(_) => {}
                    Incoming::UnsubAck(_) => {}
                    Incoming::PingReq => {}
                    Incoming::PingResp => {}
                    Incoming::Disconnect => {}
                },
                Event::Outgoing(_packet) => {}
            }
        }
    }
}

async fn handle_publish(publish: Publish, client: Arc<Mutex<Client>>, time_received: i64, mesurement: &str, topic: &str, field: &str) {
    debug!("Deserialize message into struct");
    #[derive(Deserialize, Debug)]
    struct Message {
        #[serde(rename = "X")]
        x: f64,
        #[serde(rename = "Y")]
        y: f64,
        #[serde(rename = "Z")]
        z: f64,
    }

    let Some(parsed): Option<Message> = serde_json::from_str(std::str::from_utf8(&publish.payload).unwrap()).ok() else {
        error!("Failed to deserialise this payload: {:?}", &publish.payload);
        error!("nothing sent to influx");
        return;
    };

    debug!("Construct influx data");
    let Some(point) = DataPoint::builder("accelerometer")
        // .tag("topic", topic)
        .field("x", parsed.x)
        .field("y", parsed.y)
        .field("z", parsed.z)
        .timestamp(time_received)
        .build().ok() else {
        error!("Failed to construct influx data");
        error!("nothing sent to influx");
        return;
    };

    match client
        .lock()
        .await
        .write(env!("INFLUXDB_BUCKET"), stream::iter(vec![point]))
        .await
    {
        Ok(_) => {}
        Err(err) => {
            error!("failed to send to influx, {}", err);
        }
    }
}



