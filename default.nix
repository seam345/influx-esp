let
pkgs = import <nixpkgs> {};
in
  let
    package = { pkgs, fetchFromGitLab, stdenv, cargoSha, versionTag, ... }:
      pkgs.rustPlatform.buildRustPackage rec {
        pname = "victron-influx-importer";
        version = "${versionTag}";

#        src = builtins.filterSource
#                (path: type: type != "directory" || baseNameOf path != "target")
#                ./.;

        src = fetchFromGitLab {
          domain = "gitlab.com";
          owner = "seam345";
          repo = pname;
          rev = "bc118cc";
          sha256 = "sha256-UZGOQY57clOQkhL52nHPBwltm5fEVve5csEojUvt4/M=";
        };
        cargoSha256 = "${cargoSha}";

        # compile time env variables
        INFLUXDB_HOST="http://100.89.209.13:8086";
        INFLUXDB_BUCKET="Test";
        INFLUXDB_ORG="Test";
        MQTT_ANNOUNCE_NAME = "influx-solar-power-ingester-test";
        MQTT_HOST = "100.82.216.40";
      };
  in
    with pkgs; {
      victron-influx-importer =
        callPackage package {
          versionTag= "v0.1";
          cargoSha = "sha256-9TWsHv0Lb9qpprxTqyJmhuFI4UbjPnO4gy5OJEL0Z2c=";
        };
    }
